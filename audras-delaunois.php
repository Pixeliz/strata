<!DOCTYPE HTML>
<html>
	<?php include 'inc/head.php'; ?>

	<?php $page = 'realisations'; ?>

	<?php include 'inc/header.php'; ?>

		<!-- Main -->
			<div id="main">

				<!-- One -->
				<section id="pitch">
					<header class="major">
						<h2>Audras &amp; Delaunois<a href="realisations.php"><i class="fa fa-close droite"></i></a></h2>
						<a href="www.audras-delaunois.com" target="blank">www.audras-delaunois.com</a>
					</header>
					<p>Refonte du site de cette agence immobilière Grenobloise. Travail principalement axé sur l'ergonomie et l'expérience utilisateur. L'interface est intuitive et facile à utiliser malgré la densité des informations que contient chaque page. Le site est entièrement responsive et s'adapte à tous les supports mobiles (tablettes et téléphones). Projet réalisé au sein de l'agence Groupe Curious communication.<br>
					<span class="tags">Ergonomie | Webdesign | Intégration front-end | Responsive</span></p>
					<img src="images/portfolio/audras-delaunois/audras-delaunois.jpg" alt="">
				</section>
				
			</div>

		<?php include 'inc/footer.php'; ?>

	</body>
</html>