<!DOCTYPE HTML>
<html>
	<?php include 'inc/head.php'; ?>

	<?php $page = 'realisations'; ?>

	<?php include 'inc/header.php'; ?>

		<!-- Main -->
			<div id="main">

				<!-- One -->
				<section id="pitch">
					<header class="major">
						<h2>Chembioscreen <a href="realisations.php"><i class="fa fa-close droite"></i></a></h2>
						<a href="http://www.chembioscreen.fr" target="blank">www.chembioscreen.fr</a>
					</header>
					<p>Chembioscreen est un groupement de recherche qui réunit de nombreux acteurs de la recherche, plus particulièrement en chemobiologie. Le groupement avait besoin d'un annuaire en ligne pour répertorier et localiser les chercheurs et différentes plateformes techniques. Le site est entièrement responsive et s'adapte aux écrans des tablettes et smartphones. Projet réalisé au sein de l'agence Groupe Curious communication.<br>
					<span class="tags">Webdesign | Intégration front-end | Responsive</span></p>
					<img src="images/portfolio/chembioscreen/chembioscreen.jpg" alt="Chembioscreen">
				</section>

			</div>

		<?php include 'inc/footer.php'; ?>

	</body>
</html>