<!DOCTYPE HTML>
<html>
	<?php include 'inc/head.php'; ?>

	<?php $page = 'contact'; ?>

	<?php include 'inc/header.php'; ?>

		<!-- Main -->
			<div id="main">

			<!-- Contact -->
				<section id="contact">
					<header class="major">
						<h2>Contactons-nous</h2>
					</header>
					<br>
					<p>Vous souhaitez parler de votre projet ou faire une demande de devis ? Vous êtes au bon endroit. Utilisez le formulaire ou contactons-nous par téléphone (de 9h00 à 18h00).</p>
	<!-- 				<div class="row">
						<div class="8u 12u$(small)">
							<form method="post" action="message-envoye.php">
								<div class="row uniform 50%">
									<div class="6u 12u$(xsmall)"><input type="text" name="nom" id="nom" placeholder="Nom" /></div>
									<div class="6u$ 12u$(xsmall)"><input type="email" name="email" id="email" placeholder="Email" /></div>
									<div class="12u$"><textarea name="message" id="message" placeholder="Message" rows="4"></textarea></div>
								</div>
								<br>
								<ul class="actions">
									<li><input type="submit" value="Envoyer" /></li>
								</ul>
							</form>
						</div> -->
						<div class="4u$ 12u$(small)">
							<ul class="labeled-icons">
								<li>
									<h3 class="icon fa-mobile"><span class="label">Téléphone</span></h3>
									06 60 76 47 87
								</li>
								<li>
									<h3 class="icon fa-envelope-o"><span class="label">Email</span></h3>
									<a href="mailto:cedric@coustellie.com">cedric@coustellie.com</a>
								</li>
							</ul>
						</div>
					</div>


		<?php include 'inc/footer.php'; ?>

	</body>
</html>