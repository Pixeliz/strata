<!DOCTYPE HTML>
<html>
	<?php include 'inc/head.php'; ?>

	<?php $page = 'prestations'; ?>

	<?php include 'inc/header.php'; ?>

		<!-- Main -->
			<div id="main">

				<!-- Two -->
					<section id="two">
						<header class="major">
							<h2>Concentrez-vous sur votre métier,<br />
							je m'occupe de votre site web.</h2>
						</header>
						<br>
						<p>Voici les principales prestations web que je propose. Elles sont données à titre indicatif et <b>dépendent bien entendu de la nature votre projet</b>.
							C'est pourquoi je vous invite à me contacter par téléphone au 06 60 76 47 87 ou à utiliser <a href="contact.php">le formulaire de contact</a> pour que nous échangions plus en détail sur vos besoins.</p>
						<h2>Webdesign et intégration - <i class="tags">Les formules</i></h2>
						<div class="row">
							<article class="6u 12u$(xsmall) work-item">
								<img class="image fit thumb" src="images/fulls/formule-statique.jpg" alt="" />
								<h3>Page statique - Taille S<div class="droite"><span class="small">&nbsp;A partir de</span> 199<sup>€</sup></div></h3>
								<p>Pour présenter les informations minimums et avoir une présence sur internet. 
									Mini site internet de type "one page", responsive. Parfait pour les petits budgets.</p>

							</article>

							<article class="6u 12u$(xsmall) work-item">
								<img class="image fit thumb" src="images/fulls/formule-taille-M.jpg" alt="" />
								<h3>Site "Wordpress" - Taille M<div class="droite"><span class="small">&nbsp;A partir de</span> 399<sup>€</sup></div></h3>
								<p>Installation d'un CMS de type Wordpress et d'un thème Premium. Paramétrage du site et customisations graphiques dans les limites proposées par le thème installé. Les contenus sont à remplir par vos soins. Documentation de démarrage fournie.</p>
							</article>

							<article class="6u 12u$(xsmall) work-item">
								<img class="image fit thumb" src="images/fulls/formule-taille-L.jpg" alt="" />
								<h3>Site "Wordpress" - Taille L <div class="droite"><span class="small">&nbsp;A partir de</span> 899<sup>€</sup></div></h3>
								<p>Installation d'un CMS de type Wordpress et d'un thème Premium. Paramétrage du site et customisation du thème plus poussées (hors changements de structures, en sus). Remplissage des contenus fournis par vos soins, formation de démarrage incluse.</p>
							</article>							

							<article class="6u 12u$(xsmall) work-item">
								<img class="image fit thumb" src="images/fulls/ecommerce-taille-XL.jpg" alt="" />
								<h3>Site e-commerce - Taille XL <div class="droite"><span class="small">&nbsp;A partir de</span> 1399<sup>€</sup></div></h3>								
								<p>Installation de CMS dédié au e-commerce (Prestashop, WooCommerce...) et de son thème Premium. Paramétrages de la boutique et customisation du thème dans les limites proposées par le thème. Formation de démarrage incluse.</p>
							</article>	
	
						</div>

						<h2>Autres prestations- <i class="tags">Sous traitance, graphisme...</i></h2>
						<div class="row">
							<article class="6u 12u$(xsmall) work-item">
								<img class="image fit thumb" src="images/fulls/prestations-diverses.jpg" alt="" />
								<h3>Créations, sous traitance <div class="droite">Sur devis</div></h3>								
								<p>Intégration HTML / CSS de vos maquettes, refonte de sites existants, création de designs de site, sous traitance... Travail à l'heure possible. Contactons-nous pour un devis.</p>
							</article>

							<article class="6u 12u$(xsmall) work-item">
								<img class="image fit thumb" src="images/fulls/graphisme.jpg" alt="" />
								<h3>Graphisme <div class="droite"><span class="small">&nbsp;A partir de</span> de 59<sup>€</sup></div></h3>								
								<p>Cartes de visites, flyers, dépliants etc. Tarifs sur devis.</p>
							</article>

						</div>
					</section>

				<?php include 'inc/footer.php'; ?>

			</div>



	</body>
</html>