
		<!-- Footer -->
			<footer id="footer">
				<ul class="icons">
					<li><a href="mailto:cedric@coustellie.com"><span class="flaticon-mail"></span></a></li>
					<li><a href="http://www.behance.com/mrcedric" target="blank"><span class="flaticon-behance"></span></a></li>
					<li><a href="http://www.viadeo.com/fr/profile/cédric.coustellié" target="blank"><span class="flaticon-viadeo"></span></a></li>
					<li><a href="http://fr.linkedin.com/in/cedriccoustellie" target="blank"><span class="flaticon-linkedin"></span></a></li>
					<!-- <li><a href="#" class="icon fa-linkedin"><span class="label">Linkedin</span></a></li> -->
					<!-- <li><a href="#" class="icon fa-viadeo"><span class="label">Viadeo</span></a></li> -->
<!-- 					<li><a href="#" class="icon fa-behance"><span class="label">Behance</span></a></li>
					<li><a href="mailto:cedric@coustellie.com" class="icon fa-envelope-o"><span class="label">Email</span></a></li> -->
				</ul>

				<ul class="copyright">
					<li>&copy; Pixeliz 2016 </li>
					<li>Siret : en cours d'immatriculation</li>
				</ul>
			</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.poptrox.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>