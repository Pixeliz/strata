	<head>
		<title>Création de site internet - Coustellié Cédric</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<script>
		    (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
		    function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
		    e=o.createElement(i);r=o.getElementsByTagName(i)[0];
		    e.src='//www.google-analytics.com/analytics.js';
		    r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
		    ga('create','UA-38021934-1','auto');ga('send','pageview');
		</script>
	</head>