<!DOCTYPE HTML>
<html>
	<?php include 'inc/head.php'; ?>

	<?php $page = 'infos'; ?>

	<?php include 'inc/header.php'; ?>

		<!-- Main -->
			<div id="main">

				Je suis un test de Bitbucket. Et ici un test d'actualisation avec sourcetree.

				<!-- One -->
					<section id="pitch">
						<header class="major">
							<h2>Concentrez-vous sur votre métier,<br />
							je m'occupe de votre site web.</h2>
						</header>
						<br>
						<p>Votre entreprise n'a pas encore de site internet ? Votre site est trop daté visuellement ?<br>
							Avoir un site internet attractif; qui fonctionne et présente aux visiteurs tout le contenus qu'ils cherchent, est un indispensable pour toute entreprise soucieuse de la qualité de sa présence en ligne.
							En faisant appel à mes services de webdesigner freelance, vous vous concentrez sur votre métier pendant que je m'occupe de votre image sur internet. Voici quelques unes des compétences que je peux mettre au service de votre activité.<br>
						</p>

						<div class="row">
							<article class="3u 12u$(small) work-item align-center">
								<a href="creation-site-internet.php"><span class="fa fa-pencil-square-o fa-3x"></span><br>
								<h3>Webdesign</h3></a>
								<p>Réalisation graphiques de votre site sous Photoshop, refontes, améliorations...</p>
							</article>
							<article class="3u 12u$(small) work-item align-center">
								<a href="creation-site-internet.php"><span class="fa fa-code fa-3x"></span><br>
								<h3>Intégration Front-end</h3></a>
								<p>Intégration des maquettes de votre site. HTML / CSS / JQuery</p>
							</article>
							<article class="3u 12u$(small) work-item align-center">
								<a href="creation-site-internet.php"><span class="fa fa-wordpress fa-3x"></span><br>
								<h3>CMS (Wordpress)</h3></a>
								<p>Installation et paramétrages des principaux CMS : Wordpress, Joomla...</p>
							</article>
							<article class="3u 12u$(small) work-item align-center">
								<a href="creation-site-internet.php"><span class="fa fa-shopping-cart fa-3x"></span><br>
								<h3>E-commerce</h3></a>
								<p>Installation et paramétrages des principaux CMS dédiés : Woocommerce, Prestashop</p>
							</article>
						</div>
						<br>
						<a href="creation-site-internet.php" class="button appel-accueil">Continuer vers les prestations</a>
					</section>

		<?php include 'inc/footer.php'; ?>

	</body>
</html>