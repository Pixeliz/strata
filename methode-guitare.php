<!DOCTYPE HTML>
<html>
	<?php include 'inc/head.php'; ?>

	<?php $page = 'realisations'; ?>

	<?php include 'inc/header.php'; ?>

		<!-- Main -->
			<div id="main">

				<!-- One -->
				<section id="pitch">
					<header class="major">
						<h2>Méthode guitare <a href="realisations.php"><i class="fa fa-close droite"></i></a></h2>
						<a href="http://www.methodeguitare.fr" target="blank">www.methodeguitare.fr</a>
					</header>
					<p>Site français dédié à l'apprentissage de la guitare et au tests des méthode de guitares qui existent. Il propose des cours de guitare gratuits, un catalogue de produits dédiés ainsi qu'un espace blog et une chaine Youtube. De nombreuses ressources gratuites sont proposées : impression de tablatures vierges, outils pour la pratique instrumentale...<br>
					<span class="tags">Wordpress | Webdesign | Gestion de mailing-liste | Rédaction de contenus | Page Facebook | Chaine youtube | Montage Vidéo</span></p>
					<img src="images/portfolio/methodeguitare/methode.jpg" alt="">
				</section>
				
			</div>

		<?php include 'inc/footer.php'; ?>

	</body>
</html>