<!DOCTYPE HTML>
<html>
	<?php include 'inc/head.php'; ?>

	<?php $page = 'realisations'; ?>

	<?php include 'inc/header.php'; ?>

		<!-- Main -->
			<div id="main">

				<!-- One -->
				<section id="pitch">
					<header class="major">
						<h2>Perles de sagesse <a href="realisations.php"><i class="fa fa-close droite"></i></a></h2>
						<a href="http://www.perles-de-sagesse.fr" target="blank">www.perles-de-sagesse.fr</a>
					</header>
					<p>Le thème du site tourne autour du developpement personnel et du life-styling. Il propose les biographies des meilleurs auteurs et conférenciers du genre, ainsi qu'une section blog qui publie régulièrement des citations inspirantes à partager sur les réseaux sociaux.<br>
					<span class="tags">Wordpress | Webdesign | Gestion de mailing-liste | Rédaction de contenus | Page Facebook</span></p>
					<img src="images/portfolio/perles/perles.jpg" alt="">
				</section>

			</div>

		<?php include 'inc/footer.php'; ?>

	</body>
</html>