<!DOCTYPE HTML>
<html>
	<?php include 'inc/head.php'; ?>

	<?php $page = 'realisations'; ?>

	<?php include 'inc/header.php'; ?>

		<!-- Main -->
			<div id="main">

				<!-- Three -->
					<section id="three">
						<h1>Réalisations</h1>
						<div class="row">
							<!-- Perles -->
							<article class="6u 12u$(xsmall) work-item">
								<a href="perles-de-sagesse.php" class="image fit thumb"><img src="images/portfolio/perles/miniature-perles-de-sagesse.jpg" alt="" /></a>
								<h3><a href="perles-de-sagesse.php">Perles de sagesse</a> <a class="droite" href="http://www.perles-de-sagesse.fr" target="blank"><i class="fa fa-link"></i></a></h3>
								<p class="tags">Wordpress | Webdesign | Gestion de mailing-liste | Rédaction de contenus | Page Facebook</p>
							</article>
							<!-- Methode guitare -->
							<article class="6u 12u$(xsmall) work-item">
								<a href="methode-guitare.php" class="image fit thumb"><img src="images/portfolio/methodeguitare/methode-guitare.jpg" alt="" /></a>
								<h3><a href="methode-guitare.php">Méthode Guitare</a> <a class="droite" href="http://www.methodeguitare.fr" target="blank"><i class="fa fa-link"></i></a></h3>
								<p class="tags">Wordpress | Webdesign | Gestion de mailing-liste | Rédaction de contenus | Page Facebook | Chaine youtube | Montage Vidéo</p>
							</article>
							<!-- Audras Delaunois -->
							<article class="6u 12u$(xsmall) work-item">
								<a href="audras-delaunois.php" class="image fit thumb"><img src="images/portfolio/audras-delaunois/audras-delaunois-sandrine-riviere.jpg" alt="" /></a>
								<h3><a href="audras-delaunois.php">Audras &amp; Delaunois</a><a class="droite" href="http://www.audras-delaunois.com" target="blank"><i class="fa fa-link"></i></a></h3>
								<p class="tags">Ergonomie | Webdesign | Intégration front-end | Responsive</p>
							</article>
							<!-- Urgence 114 -->
							<article class="6u$ 12u$(xsmall) work-item">
								<a href="urgence114.php" class="image fit thumb"><img src="images/portfolio/urgence114/miniature-urgence114.jpg" alt="" /></a>
								<h3><a href="urgence114.php">Urgence114</a><a class="droite" href="http://www.urgence114.fr" target="blank"><i class="fa fa-link"></i></a></h3>
								<p class="tags">Wordpress | Webdesign | Ergonomie | Création de thème sur mesure | Responsive</p>
							</article>
							<!-- Chembioscreen -->
							<article class="6u 12u$(xsmall) work-item">
								<a href="chembioscreen.php" class="image fit thumb"><img src="images/portfolio/chembioscreen/miniature-chembioscreen.jpg" alt="" /></a>
								<h3><a href="chembioscreen.php">Chembioscreen</a><a class="droite" href="http://www.chembioscreen.fr" target="blank"><i class="fa fa-link"></i></a></h3>
								<p class="tags">Webdesign | Intégration front-end | Responsive</p>
							</article>
							<!-- Pub -->
							<article class="6u$ 12u$(xsmall) work-item">
								<a href="contact.php" class="image fit thumb"><img src="images/portfolio/pub/votre-site-internet.jpg" alt="" /></a>
								<h3>Contactons-nous</h3>
								<p>Envoyez-moi un mail ou un SMS, réponse assurée sous 24h.</p>
							</article>
						</div>
					</section>

				<?php include 'inc/footer.php'; ?>

			</div>



	</body>
</html>