<!DOCTYPE HTML>
<html>
	<?php include 'inc/head.php'; ?>

	<?php $page = 'realisations'; ?>

	<?php include 'inc/header.php'; ?>

		<!-- Main -->
			<div id="main">

				<!-- One -->
				<section id="pitch">
					<header class="major">
						<h2>Urgence114 <a href="realisations.php"><i class="fa fa-close droite"></i></a></h2>
						<a href="http://www.urgence114.fr" target="blank">www.urgence114.fr</a>
					</header>
					<p>Numéro d’urgence national uniquement accessible par SMS ou Fax, pour les personnes avec des difficultés à entendre ou à parler. Refonte du site avec un travail principalement axé sur l'ergonomie et la mise en valeur des vidéos, traductions en langue des signes des contenus. Le site est entièrement responsive. Projet réalisé au sein de l'agence Groupe Curious communication.<br>
					<span class="tags">Wordpress | Webdesign | Ergonomie | Création de thème sur mesure | Responsive</span></p>
					<img src="images/portfolio/urgence114/urgence114.jpg" alt="">
				</section>
				
			</div>

		<?php include 'inc/footer.php'; ?>

	</body>
</html>